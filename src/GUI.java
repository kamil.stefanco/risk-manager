import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class GUI implements ActionListener {

    private JFrame frame;
    private JPanel panel;
    private JButton button;
    private JButton button2;
    private JLabel oneR;
    private JLabel stoploss;
    private JLabel rr;
    private JFormattedTextField textR;
    private JFormattedTextField textR2;
    private JFormattedTextField textStoploss;

    private JFormattedTextField slText;

    private JFormattedTextField tpText;
    private JFormattedTextField entryText;
    private JPanel panel2;
    private double pos_size = 0;
    private double pos_size2 = 0;
    private String risk_reward = "";
    private JLabel result;
    private JLabel result2;

    public GUI() {

        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error setting up UIManager", "Error", JOptionPane.ERROR_MESSAGE);
        }

        frame = new JFrame("Risk Manager");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,350);
        frame.setMinimumSize(new Dimension(600, 350));

        panel_pos_size();

        panel_with_prices();

        frame.add(panel,BorderLayout.WEST);
        frame.setVisible(true);
    }

    private void panel_pos_size() {
        // panel
        panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        panel.setBorder(BorderFactory.createTitledBorder("Position size calculator"));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5,5,5,5);

        // 1R
        oneR = new JLabel("1R: ");
        oneR.setToolTipText("Enter the dollar amount that you want to risk");
        textR = new JFormattedTextField(NumberFormat.getNumberInstance());
        textR.setMinimumSize(new Dimension(180, 30));
        textR.setPreferredSize(new Dimension(180, 30));
        textR.setMaximumSize(new Dimension(180, 30));
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(oneR,gbc);
        gbc.gridx = 1;
        gbc.gridy = 0;
        panel.add(textR,gbc);

        // stoploss
        stoploss = new JLabel("Stoploss (%): ");
        stoploss.setToolTipText("Enter your stoploss in %");
        textStoploss = new JFormattedTextField(NumberFormat.getNumberInstance());
        textStoploss.setMinimumSize(new Dimension(180, 30));
        textStoploss.setPreferredSize(new Dimension(180, 30));
        textStoploss.setMaximumSize(new Dimension(180, 30));
        gbc.gridx = 0;
        gbc.gridy = 1;
        panel.add(stoploss,gbc);
        gbc.gridx = 1;
        gbc.gridy = 1;
        panel.add(textStoploss,gbc);

        // button
        button = new JButton("Calculate");
        button.setToolTipText("Click to calculate the position size");
        button.addActionListener(this);
        gbc.gridx = 1;
        gbc.gridy = 2;
        panel.add(button,gbc);


        result = new JLabel("Position size: " +pos_size +"$");
        result.setFont(new Font("Arial", Font.BOLD, 16));
        gbc.gridx = 1;
        gbc.gridy = 3;
        panel.add(result,gbc);
    }

    private void panel_with_prices() {
        panel2 = new JPanel();
        panel2.setLayout(new GridBagLayout());
        panel2.setBorder(BorderFactory.createTitledBorder("Position size calculator with prices"));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5,5,5,5);

        // entry
        JLabel entry = new JLabel("Entry: ");
        entry.setToolTipText("Enter your entry price");
        entryText = new JFormattedTextField(NumberFormat.getNumberInstance());
        entryText.setMinimumSize(new Dimension(180, 30));
        entryText.setPreferredSize(new Dimension(180, 30));
        entryText.setMaximumSize(new Dimension(180, 30));
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel2.add(entry,gbc);
        gbc.gridx = 1;
        gbc.gridy = 0;
        panel2.add(entryText,gbc);

        // TP
        JLabel tp = new JLabel("TP: ");
        tp.setToolTipText("Enter take profit price");
        tpText = new JFormattedTextField(NumberFormat.getNumberInstance());
        tpText.setMinimumSize(new Dimension(180, 30));
        tpText.setPreferredSize(new Dimension(180, 30));
        tpText.setMaximumSize(new Dimension(180, 30));
        gbc.gridx = 0;
        gbc.gridy = 1;
        panel2.add(tp,gbc);
        gbc.gridx = 1;
        gbc.gridy = 1;
        panel2.add(tpText,gbc);

        // SL
        JLabel sl = new JLabel("SL: ");
        sl.setToolTipText("Enter your stoploss price in dollars");
        slText = new JFormattedTextField(NumberFormat.getNumberInstance());
        slText.setMinimumSize(new Dimension(180, 30));
        slText.setPreferredSize(new Dimension(180, 30));
        slText.setMaximumSize(new Dimension(180, 30));
        gbc.gridx = 0;
        gbc.gridy = 2;
        panel2.add(sl,gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        panel2.add(slText,gbc);

        // 1R
        JLabel r = new JLabel("1R: ");
        r.setToolTipText("Enter the dollar amount that you want to risk");
        textR2 = new JFormattedTextField(NumberFormat.getNumberInstance());
        textR2.setMinimumSize(new Dimension(180, 30));
        textR2.setPreferredSize(new Dimension(180, 30));
        textR2.setMaximumSize(new Dimension(180, 30));
        gbc.gridx = 0;
        gbc.gridy = 3;
        panel2.add(r,gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        panel2.add(textR2,gbc);

        // button
        button2 = new JButton("Calculate");
        button2.addActionListener(this);
        gbc.gridx = 1;
        gbc.gridy = 4;
        panel2.add(button2,gbc);


        result2 = new JLabel("Position size: " +pos_size2 +"$");
        result2.setFont(new Font("Arial", Font.BOLD, 16));
        gbc.gridx = 1;
        gbc.gridy = 5;
        panel2.add(result2,gbc);

        rr = new JLabel("RR: " +risk_reward);
        rr.setFont(new Font("Arial", Font.BOLD, 16));
        rr.setToolTipText("Risk to reward ratio of your trade");
        gbc.gridx = 1;
        gbc.gridy = 6;
        panel2.add(rr,gbc);

        frame.add(panel2,BorderLayout.EAST);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == button){
            calculate_pos_size();
        }
        else if(e.getSource() == button2){
            calculate_pos_size_sl_tp();
        }

    }

    private void calculate_pos_size_sl_tp() {
        double entry = ((Number) entryText.getValue()).doubleValue();
        double risk = ((Number) textR2.getValue()).doubleValue();
        double sl = ((Number) slText.getValue()).doubleValue();
        double tp = ((Number) tpText.getValue()).doubleValue();

        if(entry<sl && entry< tp){
            JOptionPane.showMessageDialog(null, "Warning: entry < sl and entry < tp", "Bad input", JOptionPane.WARNING_MESSAGE);
            return;
        } else if (entry > sl && entry>tp) {
            JOptionPane.showMessageDialog(null, "Warning: entry > sl and entry > tp", "Bad input", JOptionPane.WARNING_MESSAGE);
            return;
        } else if (entry == tp || entry == sl) {
            JOptionPane.showMessageDialog(null, "Warning: stoploss or take profit is the same as entry", "Bad input", JOptionPane.WARNING_MESSAGE);
            return;
        }

        double sl_percent = ((entry-sl)/entry)*100;
        double tp_percent = ((tp-entry)/entry)*100;

        pos_size2 = ((risk/sl_percent)*100);

        if(pos_size2 <0){
            pos_size2 = pos_size2*-1;
        }

        DecimalFormat df = new DecimalFormat("#.##");

        risk_reward = "1:" + df.format((tp_percent/sl_percent));
        result2.setText("Position size: " +df.format(pos_size2) +"$");
        rr.setText("RR " +risk_reward);
    }

    private void calculate_pos_size() {
        double risk = ((Number) textR.getValue()).doubleValue();
        double sl = ((Number)textStoploss.getValue()).doubleValue();

        pos_size = ((risk/sl)*100);

        if(pos_size <0){
            pos_size = pos_size*-1;
        }

        DecimalFormat df = new DecimalFormat("#.##");
        result.setText("Position size: " +df.format(pos_size) +"$");
    }
}
